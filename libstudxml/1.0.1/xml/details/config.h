/* xml/details/config.h.  Generated from config.h.in by configure.  */
/* file      : xml/details/config.h.in
 * copyright : Copyright (c) 2013-2014 Code Synthesis Tools CC
 * license   : MIT; see accompanying LICENSE file
 */

/* This file is automatically processed by configure. */

#ifndef XML_DETAILS_CONFIG_H
#define XML_DETAILS_CONFIG_H

/* #undef LIBSTUDXML_STATIC_LIB */
/* #undef LIBSTUDXML_EXTERNAL_EXPAT */
#define LIBSTUDXML_BYTEORDER 1234

#endif /* XML_DETAILS_CONFIG_H */
