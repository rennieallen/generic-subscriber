/*
/    (c) Copyright 2018, Real-Time Innovations, All rights reserved.           /
/                                                                              /
/ RTI grants Licensee a license to use, modify, compile, and create            / 
/ derivative works of the software solely for use with RTI Connext DDS.        / 
/ Licensee may redistribute copies of the software provided that all such      /
/ copies are subject to this license. The software is provided "as is", with   /
/ no warranty of any type, including any warranty for fitness for any purpose. /
/ RTI is under no obligation to maintain or support the software.  RTI shall   /
/ not be liable for any incidental or consequential damages arising out of the /
/ use or inability to use the software.                                        / 
*/
#include "GenericSubscriber.h"
#include "value_as_string.h"
#include <dds/topic/BuiltinTopic.hpp>
#include <optional>
#include <rti/config/Version.hpp>
#include <rti/core/ListenerBinder.hpp>
#include <rti/topic/PrintFormat.hpp>
#include <rti/topic/rtitopic.hpp>

using namespace dds::core;
using namespace dds::core::status;
using namespace dds::domain;
using namespace dds::topic;
using namespace dds::sub;
using namespace dds::pub;

using kind = xtypes::TypeKind_def;
/**
 * @brief GenericSubscriber class.  Can be instantiated with only a participant or 
 * with a participant and a type. If instantiated without a type then DDS pro is 
 * assumed and the type will be discovered.
 * 
 */
class GenericSubscriber::GenericSubscriberImpl: public NoOpDataReaderListener<xtypes::DynamicData> 
{
    private:
        using GenericTopic = Topic<xtypes::DynamicData>;
        using GenericReader = DataReader<xtypes::DynamicData>;
        using TopicName = std::string;
        using TopicType = optional<xtypes::DynamicType>;
        using TopicAndType = std::tuple<TopicName, TopicType>;
        using TopicAndTypeList = std::vector<TopicAndType>;
        using TopicColumnName = std::string;
        using TopicColumnType = xtypes::DynamicType;
        using TopicColumnKey = bool;
        using TopicColumnOptional = bool;
        using TopicColumn = std::tuple<TopicColumnName, TopicColumnType, TopicColumnKey, TopicColumnOptional>;
        using TopicColumnList = std::vector<TopicColumn>;


        GenericSubscriberImpl (const GenericSubscriberImpl&) = delete; // non construction-copyable
        GenericSubscriberImpl& operator=( const GenericSubscriberImpl& ) = delete; // non copyable
        
        // class used to gather topics from default publisher
        class MyReaderListener: public NoOpDataReaderListener<PublicationBuiltinTopicData> {
            public:
                explicit MyReaderListener(TopicAndTypeList& topics):m_topics(topics)
                {                    
                }

                // incoming data on the builtin topic reader
                void on_data_available(DataReader<PublicationBuiltinTopicData>& rdr) override
                {
                    LoanedSamples<PublicationBuiltinTopicData> samples = rdr.take();
                    for (auto& sample: samples) {
                        if (sample.info().valid()) {
                            m_topics.emplace_back(sample.data().topic_name(), sample.data()->type());
                        }
                    }
                }

            private:
                TopicAndTypeList& m_topics;
        };

        // wait until the list of topics has been populated
        void wait_for_discovery() const
        {
            while(m_discovered_topics.empty()) {
                rti::util::sleep(Duration::from_secs(poll_seconds));
            }
        }

        template<typename Funct>
        void process_members(const xtypes::StructType& record, Funct func) const
        {
            for(auto& member: record.members()) {
                func(member);

            }
        }

        // get all of the columns (i.e. members) of the topic type
        void get_topic_columns(const TopicType& topic_type)
        {
            if(!m_columns.empty()) {
                return;
            }

            auto& record = static_cast<const xtypes::StructType&>(topic_type.get());

            if(xtypes::is_extensible(record) && record.has_parent()) {
                // get the base members since this is an extensible type
                process_members(record.parent(), 
                                [this](const xtypes::Member& member) {
                                    m_columns.emplace_back(member.name(), member.type(), 
                                                            member.is_key(), member.is_optional());        
                                });
            }
            process_members(record, 
                            [this](const xtypes::Member& member) {
                                m_columns.emplace_back(member.name(), member.type(), 
                                                        member.is_key(), member.is_optional());        
                            });
        }

        std::string padding(size_t size, const char c)
        {
            return std::string(size, c);
        }

        // incoming data from the topic we subscribed to
        void on_data_available(DataReader<xtypes::DynamicData>& rdr) override
        {
            LoanedSamples<xtypes::DynamicData> samples = rdr.take();

            for (auto& sample: samples)  {
                if (sample.info().valid()) {
                    for(auto& column: m_columns) {
                        int column_width=(std::get<1>(column).kind() == kind::SEQUENCE_TYPE || 
                                           std::get<1>(column).kind() == kind::AGGREGATION_TYPE ||
                                           std::get<1>(column).kind() == kind::STRUCTURE_TYPE)?data_row_agg_width:
                                                                                               data_row_sim_width;
                        std::cout << padding(column_width-data_row_width_off, ' ') 
                                  << std::setw(column_width)
                                  << value_as_string(sample.data(), std::get<0>(column));

                    }
                    std::cout << std::endl;
                }
            }
        }

        // print out header based on discovered topic type members
        void header(const TopicType& topic_type)
        {
            get_topic_columns(topic_type);

            std::cout << " ";
            for(auto& column: m_columns) {
                int column_width=(std::get<1>(column).kind() == kind::SEQUENCE_TYPE || 
                                   std::get<1>(column).kind() == kind::AGGREGATION_TYPE || 
                                   std::get<1>(column).kind() == kind::STRUCTURE_TYPE)?hdr_row_agg_width:
                                                                                       hdr_row_sim_width;
                std::cout << std::setw(column_width) 
                          << std::get<0>(column) 
                          << padding(column_width-hdr_row_width_off, ' ') << "|";
            }
            std::cout << std::endl; 
        }
 
    public:
        GenericSubscriberImpl(DomainParticipant& participant, bool verbose): m_participant(participant),
                                                                             m_ear(m_discovered_topics),
                                                                             m_verbose(verbose)
        {
            std::vector<DataReader<PublicationBuiltinTopicData>> rdr_list;

            // locate the built-in subscriber for the topic names
            find<DataReader<PublicationBuiltinTopicData>>(builtin_subscriber(m_participant), 
                                                          dds::topic::publication_topic_name(), 
                                                          std::back_inserter(rdr_list));
            // attach listener to all readers
            for(auto& rdr: rdr_list) {
                rdr.listener(&m_ear.value(), StatusMask::data_available());
            }

            // only now that we have our listeners attached are we ready to enable the participant
            m_participant.enable();
        }

        GenericSubscriberImpl(DomainParticipant& participant,  
                              const xtypes::StructType& topic_type, 
                              bool verbose): m_topic_type(topic_type), 
                                             m_participant(participant),
                                             m_verbose(verbose)
        {
            m_participant.enable();
        }

        void list_topics()
        {
            wait_for_discovery();
            
            std::cout << "Number of topics discovered: " << m_discovered_topics.size() << std::endl;
            for(auto topic: m_discovered_topics) {
                std::cout << "Discovered topic: " << std::get<0>(topic) << std::endl;
            }
        }

        void receive(const TopicName& topic_name)
        {
            TopicType topic_type;

            if(m_ear) {
                wait_for_discovery();

                for(auto topic: m_discovered_topics) {
                    if(std::get<0>(topic) == topic_name) {
                        if((topic_type = std::get<1>(topic))) {
                            std::cout   << "Enabling subscribing on topic " << topic_name
                                        << " which is of type " << topic_type.get().name() << std::endl;

                            break;  // only support subscribing to a single topic
                        }
                    }
                }

                if(!topic_type) {
                    throw TopicNotFound(topic_name);
                }
            } else {              
                topic_type = m_topic_type;
            }

            GenericReader reader(Subscriber(m_participant), 
                                            GenericTopic(m_participant, topic_name, topic_type.get()));

            if(reader.is_nil()) {
                throw ReaderNotCreated();
            }

            auto scoped_listener = rti::core::bind_listener(reader, this, 
                                                            dds::core::status::StatusMask::data_available());

            // display headers 
            header(topic_type);

            // while there are writers available collect and display data
            while(true) {
                rti::util::sleep(Duration::from_secs(poll_seconds));
            }
        }

    private:
        TopicAndTypeList                                        m_discovered_topics;
        TopicType                                               m_topic_type;
        DomainParticipant&                                      m_participant;
        std::optional<MyReaderListener>                         m_ear = std::nullopt;

        bool                                                    m_verbose;
        TopicColumnList                                         m_columns;

        static const int                                        poll_seconds = 1;

        static const uint8_t                                    data_row_agg_width = 33;
        static const uint8_t                                    data_row_sim_width = 16;
        static const uint8_t                                    hdr_row_agg_width = 30;
        static const uint8_t                                    hdr_row_sim_width = 15;
        
        static const uint8_t                                    data_row_width_off = 14;
        static const uint8_t                                    hdr_row_width_off = 8;
};

GenericSubscriber::GenericSubscriber(DomainParticipant& participant, bool verbose)
    : impl(std::make_unique<GenericSubscriberImpl>(participant, verbose))
{
}

GenericSubscriber::GenericSubscriber(DomainParticipant& participant, const xtypes::StructType& topic_type, 
                                     bool verbose)
    : impl(std::make_unique<GenericSubscriberImpl>(participant, topic_type, verbose))
{
}

GenericSubscriber::~GenericSubscriber() = default;

void GenericSubscriber::list_topics()
{
    impl->list_topics();
}

void GenericSubscriber::receive(const std::string& topic_name)
{
    impl->receive(topic_name);
}