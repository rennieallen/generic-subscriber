/*
/         (c) Copyright 2018, Real-Time Innovations, All rights reserved.      /
/                                                                              /
/ RTI grants Licensee a license to use, modify, compile, and create            / 
/ derivative works of the software solely for use with RTI Connext DDS.        / 
/ Licensee may redistribute copies of the software provided that all such      /
/ copies are subject to this license. The software is provided "as is", with   /
/ no warranty of any type, including any warranty for fitness for any purpose. /
/ RTI is under no obligation to maintain or support the software.  RTI shall   /
/ not be liable for any incidental or consequential damages arising out of the /
/ use or inability to use the software.                                        /
*/

#include "value_as_string.h"
#include <sstream>
#include <string>
using namespace dds::core;

static std::string do_text_conversion(const rti::core::xtypes::DynamicDataMemberInfo& info, 
                        uint32_t member_index, 
                        const xtypes::DynamicData& data,
                        bool add_name);

static std::string decode_sequence(const xtypes::DynamicData& data)
{
    std::stringstream buffer;
    int num_members = data.member_count();

    for(int32_t i = 1; i <= num_members; ++i) {
        buffer << " " << do_text_conversion(data.member_info(i), i, data, true);
    }

    return buffer.str();
}

static std::string do_text_conversion(const rti::core::xtypes::DynamicDataMemberInfo& info, 
                                      uint32_t member_index, 
                                      const xtypes::DynamicData& data,
                                      bool add_name)
{
    std::stringstream buffer;
    std::string name=(add_name && (info.member_name().c_str() != nullptr))?(info.member_name().to_std_string()+"="):"";

    switch(info.member_kind().underlying()) {
        case xtypes::TypeKind_def::BOOLEAN_TYPE:
            buffer << name << data.value<bool>(member_index);
            return buffer.str();                 
        case xtypes::TypeKind_def::UINT_8_TYPE: 
            buffer << name << static_cast<uint32_t>(data.value<uint8_t>(member_index));
            return buffer.str();                 
        case xtypes::TypeKind_def::INT_16_TYPE: 
            buffer << name << data.value<int16_t>(member_index);
            return buffer.str();                 
        case xtypes::TypeKind_def::UINT_16_TYPE: 
            buffer << name << data.value<uint16_t>(member_index);
            return buffer.str();                 
        case xtypes::TypeKind_def::INT_32_TYPE: 
            buffer << name << data.value<int32_t>(member_index);
            return buffer.str();                 
        case xtypes::TypeKind_def::UINT_32_TYPE: 
            buffer << name << data.value<uint32_t>(member_index);
            return buffer.str();                 
        case xtypes::TypeKind_def::INT_64_TYPE: 
            buffer << name << data.value<int64_t>(member_index);
            return buffer.str();                 
        case xtypes::TypeKind_def::UINT_64_TYPE: 
            buffer << name << data.value<uint64_t>(member_index);
            return buffer.str();                 
        case xtypes::TypeKind_def::FLOAT_32_TYPE: 
            buffer << name << data.value<float>(member_index);
            return buffer.str();                 
        case xtypes::TypeKind_def::FLOAT_64_TYPE: 
            buffer << name << data.value<double>(member_index);
            return buffer.str();                 
        case xtypes::TypeKind_def::FLOAT_128_TYPE: 
            buffer << name << "Unsupported Floating point type";
            return buffer.str();                 
        case xtypes::TypeKind_def::CHAR_8_TYPE: 
            buffer << name << data.value<char>(member_index);
            return buffer.str();                 
        case xtypes::TypeKind_def::ENUMERATION_TYPE:
            buffer << name << data.value<uint32_t>(member_index);
            return buffer.str(); 
        case xtypes::TypeKind_def::STRING_TYPE: 
            buffer << name << data.value<std::string>(member_index);
            return buffer.str();                 
        case xtypes::TypeKind_def::WSTRING_TYPE: 
            return std::string();                 
        case xtypes::TypeKind_def::ARRAY_TYPE:
            return std::string();
        case xtypes::TypeKind_def::UNION_TYPE: 
        case xtypes::TypeKind_def::STRUCTURE_TYPE:  
        case xtypes::TypeKind_def::SEQUENCE_TYPE:
            return decode_sequence(data.value<xtypes::DynamicData>(member_index));
        default:
            return std::string();
    }
}

// represent given dynamic data value as a string
std::string value_as_string(const xtypes::DynamicData& data, const std::string& member_name)
{
    return do_text_conversion(data.member_info(member_name), 
                              data.member_info(member_name).member_index(), 
                              data,
                              false);
}

