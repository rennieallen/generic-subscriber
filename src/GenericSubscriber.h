/********************************************************************************/
/*         (c) Copyright 2018, Real-Time Innovations, All rights reserved.      */
/*                                                                              */
/* RTI grants Licensee a license to use, modify, compile, and create            */ 
/* derivative works of the software solely for use with RTI Connext DDS.        */ 
/* Licensee may redistribute copies of the software provided that all such      */
/* copies are subject to this license. The software is provided "as is", with   */
/* no warranty of any type, including any warranty for fitness for any purpose. */
/* RTI is under no obligation to maintain or support the software.  RTI shall   */
/* not be liable for any incidental or consequential damages arising out of the */
/* use or inability to use the software.                                        */
/********************************************************************************/
#ifndef __GENERIC_SUBSCRIBER__
#define __GENERIC_SUBSCRIBER__

#include <dds/dds.hpp>

#include <string>

/**
 * A GenericSubscriber requires the factory to be configured to
 * create entities in a disabled state prior to instantiation.  
 * It will not function otherwise.
 */
class GenericSubscriber {
public:
    /**
     * @brief Exception thrown when client supplied topic name can not be found in domain
     * 
     */
    class TopicNotFound: std::exception
    {
        public:
            /**
             * @brief Construct a new Topic Not Found object
             * 
             * @param topic_name name of the topic that was not found
             */
            TopicNotFound(const std::string& topic_name): m_topic_name(topic_name)
            {
            }
            /**
             * @brief Retrieve a const char* pointing to the name of the topic that was not found
             * 
             * @return const char* 
             */
            const char* what(void) const noexcept override
            {
                return m_topic_name.c_str();
            }
        private:
            std::string m_topic_name;
    };
    /**
     * @brief Exception thrown when a reader cannot be created for the specified topic
     * 
     */
    class ReaderNotCreated: std::exception
    {
        public:
            /**
             * @brief Construct a new Reader Not Created object
             * 
             */
            ReaderNotCreated()
            {
            }
    };

    /**
     * @brief Construct a new Generic Subscriber Impl object
     * 
     * @param participant participant under which the subscriber will be created
     * @param verbose enable verbose logging
     */
    GenericSubscriber(dds::domain::DomainParticipant& participant,
                      bool verbose);

    /**
     * @brief Construct a new Generic Subscriber Impl object
     * 
     * @param participant participant under which the subscriber will be created
     * @param topic_type datatype (for the topic name which will be supplied to receive)
     * @param verbose enable verbose logging
     */
    GenericSubscriber(dds::domain::DomainParticipant& participant, 
                      const dds::core::xtypes::StructType& topic_type, 
                      bool verbose);
    /**
     * Subscriber destructor
     */
    ~GenericSubscriber(); 

    /**
     * @brief list topics available in participants domain
     * 
     */
    void list_topics(void);

    /**
     * @brief begin receiving data
     * 
     * @param topic_name topic on which to receive data
     */
    void receive(const std::string& topic_name);

private:
    class GenericSubscriberImpl;
    std::unique_ptr<GenericSubscriberImpl> impl{};
};

#endif