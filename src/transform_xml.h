/********************************************************************************/
/*         (c) Copyright 2018, Real-Time Innovations, All rights reserved.      */
/*                                                                              */
/* RTI grants Licensee a license to use, modify, compile, and create            */ 
/* derivative works of the software solely for use with RTI Connext DDS.        */ 
/* Licensee may redistribute copies of the software provided that all such      */
/* copies are subject to this license. The software is provided "as is", with   */
/* no warranty of any type, including any warranty for fitness for any purpose. */
/* RTI is under no obligation to maintain or support the software.  RTI shall   */
/* not be liable for any incidental or consequential damages arising out of the */
/* use or inability to use the software.                                        */
/********************************************************************************/
#ifndef _TRANSFORM_XML_H_INCLUDED
#define _TRANSFORM_XML_H_INCLUDED

#include <string>

/**
 * @brief transform an XML representation of a datatype as emitted by 
 * "rtiddsgen -convertToXml" to an XML format compatible with the modern C++
 * QosProvider constructor argument
 * 
 * @param filename full path to the file containing an rtiddsgen XML datatype representation
 * @return std::string full path to a converted file suitable for passing to QosProvider 
 * constructor
 */
std::string transform_xml(std::string& filename);

#endif
