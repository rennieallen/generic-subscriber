/********************************************************************************/
/*         (c) Copyright 2018, Real-Time Innovations, All rights reserved.      */
/*                                                                              */
/* RTI grants Licensee a license to use, modify, compile, and create            */ 
/* derivative works of the software solely for use with RTI Connext DDS.        */ 
/* Licensee may redistribute copies of the software provided that all such      */
/* copies are subject to this license. The software is provided "as is", with   */
/* no warranty of any type, including any warranty for fitness for any purpose. */
/* RTI is under no obligation to maintain or support the software.  RTI shall   */
/* not be liable for any incidental or consequential damages arising out of the */
/* use or inability to use the software.                                        */
/********************************************************************************/
#ifndef _VALUE_AS_STRING_H_
#define _VALUE_AS_STRING_H_

#include <dds/dds.hpp>
#include <string>

/**
 * @brief for a given member of a dynamic datatype return a std::string 
 * representation of its value
 * 
 * @param data dynamic datatype to which the member belongs 
 * @param member_name name of the member
 * @return std::string string representation of members value
 */
std::string value_as_string(const dds::core::xtypes::DynamicData& data, 
                            const std::string& member_name);

#endif