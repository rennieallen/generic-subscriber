/*
/    (c) Copyright 2018, Real-Time Innovations, All rights reserved.           /
/                                                                              /
/ RTI grants Licensee a license to use, modify, compile, and create            / 
/ derivative works of the software solely for use with RTI Connext DDS.        / 
/ Licensee may redistribute copies of the software provided that all such      /
/ copies are subject to this license. The software is provided "as is", with   /
/ no warranty of any type, including any warranty for fitness for any purpose. /
/ RTI is under no obligation to maintain or support the software.  RTI shall   /
/ not be liable for any incidental or consequential damages arising out of the /
/ use or inability to use the software.                                        / 
*/

#include "transform_xml.h"
#include <fstream>
#include <sstream>
#include <algorithm>
#include <xml/parser>
#include <xml/serializer>

using namespace xml;

struct MatchPathSeparator
{
    bool operator()( char ch ) const
    {
        return ch == '\\' || ch == '/';
    }
};

static std::string basename(std::string const& pathname )
{
    return std::string(std::find_if(pathname.rbegin(), pathname.rend(), MatchPathSeparator()).base(),
                       pathname.end() );
}

std::string transform_xml(std::string& filename)
{
    bool transforming=false;
    int  depth=0;
    std::ifstream ifs;
    ifs.exceptions (std::ifstream::badbit | std::ifstream::failbit);
    ifs.open (filename, std::ifstream::in | std::ifstream::binary);

    // Configure the parser to receive attributes as events as well
    // as to receive prefix-namespace mappings (namespace declarations
    // in XML terminology).
    //
    parser p(ifs,
             filename,
             parser::receive_default |
             parser::receive_attributes_event |
             parser::receive_namespace_decls);    

    std::stringstream type_definition;
    serializer s(type_definition, "typedef", 1);
    s.xml_decl();

    for (parser::event_type e (p.next ()); e != parser::eof; e = p.next ())
    {
      switch (e)
      {
      case parser::start_element:
        {
          if(p.qname().string() == "type_library") {
              return filename;
          } 
          if(p.qname().string() == "types") {
              qname transformed_qname("dds");
              s.start_element(transformed_qname);
              s.namespace_decl("http://www.w3.org/2001/XMLSchema-instance", "xsi");
              transforming=true;
          } else {              
              s.start_element(p.qname());
              if(transforming) {
                  ++depth;
              }  
          }
          break;
        }
      case parser::end_element:
        {
          if(transforming && depth == 0) {
            s.end_element();
            transforming=false;
          } else if(transforming) {
            depth=std::max(0,--depth);
          }
          s.end_element();
          break;
        }
      case parser::start_namespace_decl:
        {
          break;
        }
      case parser::end_namespace_decl:
        {
          // There is nothing in XML that indicates the end of namespace
          // declaration since it is scope-based.
          //
          break;
        }
      case parser::start_attribute:
        {
          s.start_attribute(p.qname());
          break;
        }
      case parser::end_attribute:
        {
          s.end_attribute();
          if(transforming && depth == 0) {
            s.start_element("type_library");
            s.start_attribute("name");
            s.characters("MyTypes");
            s.end_attribute();
            depth=1;
          }
          break;
        }
      case parser::characters:
        {
          s.characters(p.value());
          break;
        }
      }
    }
    s.end_element();

    std::string fullpath = "/tmp/" + basename(filename);
    std::ofstream file(fullpath);

    file << type_definition.str();

    file.close();

    return fullpath;
}
